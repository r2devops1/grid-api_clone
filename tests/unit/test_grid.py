#!/usr/bin/env python

""" Grid unittests """

import json
import unittest

from mock import patch
from grid.views import get_all_grid
from grid.utils import check_win

GRID = {'id': 22,
        'date_created': 'Tue, 16 Jul 2019 19:16:33 GMT',
        'date_modified': 'Tue, 16 Jul 2019 19:22:05 GMT',
        'content': ["x", "o", "x",
                    "x", "x", "o",
                    "o", "x", "o"],
        'last_player': "x",
        'winner': None}
GRID_EMPTY = {'id': 1994,
              'date_created': 'Tue, 15 Jul 2019 05:01:39 GMT',
              'date_modified': 'Tue, 15 Jul 2019 05:01:39 GMT',
              'content': [None, None, None,
                          None, None, None,
                          None, None, None],
              'last_player': "o",
              'winner': None}
GRID_LAST_PLAY = {'id': 42,
                  'date_created': 'Tue, 15 Jul 2019 05:01:39 GMT',
                  'date_modified': 'Tue, 15 Jul 2019 05:01:39 GMT',
                  'content': ["x", "", "x",
                              None, "o", None,
                              None, "o", None],
                  'last_player': "o",
                  'winner': None}

EMPTY = [None, None, None, None, None, None, None, None, None]
FULL = ["x", "o", "x", "x", "x", "o", "o", "x", "o"]
FULL_WIN = ["x", "o", "x", "o", "o", "x", "x", "o", "o"]
WIN_LIG1 = ["x", "x", "x", "o", "o", None, "o", "x", "o"]
WIN_LIG2 = ["o", "o", None, "x", "x", "x", "x", "o", "x"]
WIN_LIG3 = ["o", "o", None, "o", "x", "o", "x", "x", "x"]
WIN_COL1 = ["x", "x", None, "x", "o", "o", "x", "o", "o"]
WIN_COL2 = ["x", "x", None, "o", "x", "o", "o", "x", "o"]
WIN_COL3 = ["x", "o", "x", "o", "x", "x", "o", "o", "x"]
WIN_DIAG1 = ["x", "o", None, "o", "x", "o", "o", "x", "x"]
WIN_DIAG2 = ["o", "o", "x", "o", "x", "o", "x", None, "x"]


class FakeJsonify():
    """
    Fake jsonify flask function because
    it requires an initialized flask app
    """

    def __init__(self, data):
        self.data = json.dumps(data)
        self.status_code = None

    def dict(self):
        """ return dict representation """
        return {'status_code': self.status_code, 'data': self.data}


class FakeGrid():
    """ Fake grid model object """

    def __init__(self, grid):
        """ constructor """
        self.grid = grid

    def get_data(self):
        """ return data of a grid """
        return self.grid


class GridsTestCase(unittest.TestCase):
    """ This class represents the grids test case """

    @patch('grid.models.Grid.get_all')
    @patch('grid.views.jsonify')
    @patch('grid.DB')
    def test_get_all(self, mock_db, mock_jsonify, mock_getall):
        """ test get_all_grid function """
        grid_list = [FakeGrid(GRID),
                     FakeGrid(GRID_EMPTY)]
        mock_getall.return_value = grid_list
        mock_jsonify.side_effect = FakeJsonify
        mock_db.return_value = True
        response = get_all_grid()
        result = {'data': '[{"id": 22, "date_created": "Tue, 16 Jul 2019 19:16:33 GMT", '
                          '"date_modified": "Tue, 16 Jul 2019 19:22:05 GMT", "content": ["x", '
                          '"o", "x", "x", "x", "o", "o", "x", "o"], "last_player": "x", '
                          '"winner": null}, {"id": 1994, "date_created": '
                          '"Tue, 15 Jul 2019 05:01:39 GMT", "date_modified": "Tue, 15 Jul 2019 '
                          '05:01:39 GMT", "content": [null, null, null, null, null, null, null, '
                          'null, null], "last_player": "o", "winner": null}]',
                  'status_code': 200}
        self.assertEqual(response.dict(), result)

    @patch('grid.models.Grid.get_all')
    @patch('grid.views.jsonify')
    @patch('grid.DB')
    def test_get_all_empty(self, mock_db, mock_jsonify, mock_getall):
        """ test get_all_grid function without grid """
        grid_list = []
        mock_getall.return_value = grid_list
        mock_jsonify.side_effect = FakeJsonify
        mock_db.return_value = True
        response = get_all_grid()
        result = {'data': '[]', 'status_code': 200}
        self.assertEqual(response.dict(), result)

    # NOTE: here, stop to test views, test utils functions used in view

    def test_check_win(self):
        """ test in win case """
        self.assertTrue(check_win(WIN_COL1, 'x'))
        self.assertTrue(check_win(WIN_COL2, 'x'))
        self.assertTrue(check_win(WIN_COL3, 'x'))
        self.assertTrue(check_win(WIN_LIG1, 'x'))
        self.assertTrue(check_win(WIN_LIG2, 'x'))
        self.assertTrue(check_win(WIN_LIG3, 'x'))
        self.assertTrue(check_win(WIN_DIAG1, 'x'))
        self.assertTrue(check_win(WIN_DIAG2, 'x'))
        self.assertTrue(check_win(FULL_WIN, 'o'))

    def test_check_not_win(self):
        """ test in win case """
        self.assertFalse(check_win(WIN_COL1, 'o'))
        self.assertFalse(check_win(WIN_LIG1, 'o'))
        self.assertFalse(check_win(WIN_DIAG1, 'o'))
        self.assertFalse(check_win(EMPTY, 'o'))
        self.assertFalse(check_win(EMPTY, 'x'))
        self.assertFalse(check_win(FULL, 'x'))
        self.assertFalse(check_win(FULL, 'o'))
        self.assertFalse(check_win(FULL_WIN, 'x'))
